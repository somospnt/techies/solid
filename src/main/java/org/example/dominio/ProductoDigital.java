package org.example.dominio;

public class ProductoDigital extends Producto {
    private double tamanioEnMB;

    public ProductoDigital(String nombre, double precio, double tamanioEnMB) {
        super(nombre, precio);
        this.tamanioEnMB = tamanioEnMB;
    }

    public double getTamanioEnMB() {
        return tamanioEnMB;
    }

    public void setTamanioEnMB(double tamanioEnMB) {
        this.tamanioEnMB = tamanioEnMB;
    }
}
