package org.example.solid;

public class ProductoDigital extends Producto {
    private double tamanioEnMB;

    public ProductoDigital(String nombre, double precio, double tamanioEnMB) {
        super(nombre, precio);
        this.tamanioEnMB = tamanioEnMB;
    }

    public double getTamanioEnMB() {
        return tamanioEnMB;
    }
}
