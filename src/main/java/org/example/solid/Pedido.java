package org.example.solid;

import java.util.ArrayList;
import java.util.List;

public class Pedido {
    private Cliente cliente;
    private List<Vendible> items;

    public Pedido(Cliente cliente) {
        this.cliente = cliente;
        this.items = new ArrayList<>();
    }

    public void agregarItem(Vendible item) {
        items.add(item);
    }

    public double calcularTotal() {
        double total = 0;
        for (Vendible item : items) {
            total += item.getPrecio();
        }
        return total;
    }
}
