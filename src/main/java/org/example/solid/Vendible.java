package org.example.solid;

public interface Vendible {
    String getNombre();
    double getPrecio();
}
