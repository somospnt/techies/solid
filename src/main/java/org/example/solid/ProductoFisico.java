package org.example.solid;

public class ProductoFisico extends Producto {
    private double peso;

    public ProductoFisico(String nombre, double precio, double peso) {
        super(nombre, precio);
        this.peso = peso;
    }

    public double getPeso() {
        return peso;
    }
}
